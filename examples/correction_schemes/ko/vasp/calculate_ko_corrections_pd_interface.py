# -*- coding: utf-8 -*-
import numpy as np

import ase.io
from spinney.io.vasp import extract_potential_at_core_vasp
from spinney.structures.pointdefect import PointDefect

if __name__ == '__main__':
    # pristine system
    pristine = ase.io.read('OUTCAR_prist')

    # Specifications of the defective system: V_B -3 in BN
    q = -3 # charge state
    e_r = (4.601064 + 2.314707) # electronic and ionic contribution to dielectric
                                # constant, calculated with VASP
    a = 0.41667
    def_position = np.ones(3)*a # fractional coordinates defect
    vbm = 7.2981 # valence band maximum

    # Prepare the chemical potentials B-rich conditions
    boron = ase.io.read('OUTCAR_B')
    mu_b = boron.get_total_energy()/boron.get_number_of_atoms()

    # prepare a numpy array with information about the electrostatic potential
    pot_prist = extract_potential_at_core_vasp('OUTCAR_prist')
    pot_def = extract_potential_at_core_vasp('OUTCAR_def')

    # initialite PointDefect instance
    defective = ase.io.read('OUTCAR_def')
    pd = PointDefect(defective)
    pd.set_dielectric_tensor(e_r)
    pd.set_defect_position(def_position)
    pd.set_defect_charge(q)
    pd.set_pristine_system(pristine)
    pd.set_vbm(vbm)
    pd.set_chemical_potential_values({'N':None, 'B':mu_b}, force=True)
    # correction scheme of Kumagai and Oba
    pd.set_finite_size_correction_scheme('ko')
    pd.add_correction_scheme_data(potential_pristine=pot_prist, potential_defective=pot_def)
    print('Formation energy B-rich, uncorrected: {:.3f}'.format(
        pd.get_defect_formation_energy()))
    print('Formation energy B-rich, corrected: {:.3f}'.format(
        pd.get_defect_formation_energy(True)))

    import matplotlib
    import matplotlib.pyplot as plt

    matplotlib.rcParams.update({'font.size': 26}) 

    ecorr, dd = pd.calculate_finite_size_correction(verbose=True)
    corr_obj = dd['Corr object']
    
    plt.figure(figsize=(8,8))
    dist, pot_align_vs_dist = corr_obj.alignment_potential_vs_distance_sampling_region
    plt.scatter(dist, pot_align_vs_dist, color='green', marker='^',
                label=r'$\Delta V_{PC, q/b}$')
    dist, pot = corr_obj.ewald_potential_vs_distance_sampling_region
    plt.scatter(dist, pot, label=r'$V_{PC, q}$', color='blue')
    dist, pot = corr_obj.difference_potential_vs_distance
    plt.scatter(dist, pot, label=r'$V_{q/b}$', marker='x')
    # beginning of the sampling region
    radius = corr_obj.sphere_radius
    plt.axvspan(radius, 9, color='blue', alpha=0.3)
    plt.plot(np.linspace(1.5, 9, 100), np.zeros(100), color='black',
             linestyle='--')
    plt.legend()
    plt.xlabel(r'Distance ($\AA$)')
    plt.ylabel(r'Potential (V)')
    plt.xlim(1.5, 9)
    plt.ylim(-3, 0.5)
    plt.tight_layout()
    plt.savefig('atomic_site_potential_convergence.pdf', format='pdf')
    plt.show()
