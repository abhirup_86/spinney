import numpy as np
from ase.calculators.vasp import VaspChargeDensity
import ase.io
from spinney.structures.pointdefect import PointDefect
from spinney.defects import fnv

if __name__ == '__main__':
    prist = 'OUTCAR_prist'
    defect = 'OUTCAR_def'
    locpot_bulk = 'LOCPOT_prist'
    locpot_vo1 = 'LOCPOT_def'

    # Specifications of the defective system: V_Ga -3 in GaAs
    q = -3 # charge state
    dielectric_constant = 12.4 # from Freysoldt et al. PRL (2009) paper
    def_position = np.array([0.33333333333, 0.5, 0.5]) # fractional coordinates defect in supercell
    axis_average = 2 # axis along where the average potential is calculated
    vbm = 4.0513
    # prepare a numpy 3D array with information about the electrostatic potential
    locpot_prist = VaspChargeDensity(locpot_bulk)   # read using ase 
    supercell = locpot_prist.atoms[-1]
    locpot_def = VaspChargeDensity(locpot_vo1)
    locpot_arr_p = locpot_prist.chg[-1]*supercell.get_volume()*(-1)
    locpot_arr_def = locpot_def.chg[-1]*supercell.get_volume()*(-1)
    del locpot_prist; del locpot_def
    # model defect-induced charge density
    parcar = 'PARCHG_def'
    charge_density = VaspChargeDensity(parcar)
    charge_density = charge_density.chg[-1]

    pristine = ase.io.read('OUTCAR_prist')
    # Prepare the chemical potentials Ga-rich conditions
    gallium = ase.io.read('OUTCAR_Ga')
    mu_ga = gallium.get_total_energy()/gallium.get_number_of_atoms()

    # initialite PointDefect instance
    pd = PointDefect(ase.io.read(defect))
    pd.set_dielectric_tensor(dielectric_constant)
    pd.set_defect_position(def_position)
    pd.set_defect_charge(q)
    pd.set_pristine_system(pristine)
    pd.set_vbm(vbm)
    pd.set_finite_size_correction_scheme('fnv')
    pd.add_correction_scheme_data(potential_pristine=locpot_arr_p, potential_defective=locpot_arr_def, 
                                  axis=axis_average, defect_density=charge_density)
    pd.set_chemical_potential_values({'Ga':mu_ga, 'As':None}, force=True)
    print('Formation energy not corrected: ', pd.get_defect_formation_energy())
    print('Formation energy corrected: ', pd.get_defect_formation_energy(True))

    ecorr, dd = pd.calculate_finite_size_correction(verbose=True)
    axis_grid = dd['Potential grid']
    lr_potential = dd['Model potential']
    sr_potential = dd['Alignment potential']
    dft_potential = dd['DFT potential']
    alignment_term = dd['Alignment term']
    print(alignment_term)
    Plott = fnv.FPlotterPot(axis_grid, dft_potential, lr_potential, sr_potential,
                            -alignment_term, 'z')
    Plott.plot('VGa_-3_GaAs')
