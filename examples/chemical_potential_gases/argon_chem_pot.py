import numpy as np
import matplotlib.pyplot as plt

from spinney.thermodynamics.chempots import IdealGasChemPot

# initialize the parameters
T1 = 298; T2 = 1000; T3 = 2000; T4 = 6000
A = 20.78600
B = 2.825911e-7
C = -1.464191e-7
D = 1.092131e-8
E = -3.661371e-8
F = -6.197350
G = 179.9990
H = 0
dH_std = -6.197
parameters = [A, B, C, D, E, F, G, H]
t_parameters = [T1, T2, T3, T4]

# Initialize the new gas
argon =  IdealGasChemPot(energy_units='eV', pressure_units='Atm')
for index, attr in enumerate(argon.equation_parameters1):
    setattr(argon, attr, parameters[index])
for index, attr in enumerate(argon.equation_parameters2):
    setattr(argon, attr, parameters[index])
for index, attr in enumerate(argon.equation_parameters3):
    setattr(argon, attr, parameters[index])
for index, attr in enumerate(argon.equation_parametersT):
    setattr(argon, attr, t_parameters[index]) 
argon.dH_std = dH_std


T_range = np.linspace(300, 1000, 200)
chem_pot_vs_T = argon.get_ideal_gas_chemical_potential_Shomate(0, 1, T_range)

plt.plot(T_range, chem_pot_vs_T, linewidth=2)
plt.xlabel('T (K)')
plt.ylabel(r'$\Delta \mu_{Ar}$ (eV)')
plt.show()

pressures = [1, 10, 1e-6]
chem_pot_vs_T = argon.get_ideal_gas_chemical_potential_Shomate(0, pressures, T_range)
plt.plot(T_range, chem_pot_vs_T, linewidth=2)
plt.legend([str(p) + ' Atm' for p in pressures])
plt.xlabel('T (K)')
plt.ylabel(r'$\Delta \mu_{Ar}$ (eV)')
plt.show()
