import matplotlib.pyplot as plt
import numpy as np
import os
import ase.io
from spinney.defects.concentration import extract_formation_energies_from_file
from spinney.defects.concentration import EquilibriumConcentrations
from spinney.io.vasp import extract_dos

energy_file = 'formation_energies_GaN_Ga_rich.txt'

charge_states, form_energy_vbm = extract_formation_energies_from_file(energy_file)

data = os.path.join('..', 'diagram', 'data')
path_pristine = os.path.join(data, 'pristine', 'OUTCAR')

# Band alignment
vbm_offset = 0.85
vbm = 5.009256 - vbm_offset # align the VBM with the HSE band
e_gap = 1.713

# get the equilibrium volume of the primitive cell of GaN
ase_pristine = ase.io.read(path_pristine, format='vasp-out')
volume = ase_pristine.get_volume()/len(ase_pristine)
volume *= 4
factor = 1e-8**3 * volume
factor = 1/factor
print(factor)
# concentrations of the defective sites
site_conc = {'Ga_N':4, 'N_Ga':4, 'Vac_N':4, 'Vac_Ga':4,
             'Ga_int':6, 'N_int':6, 'electron':36 , 'hole':36}
site_conc = {key:value*factor for key, value in site_conc.items()}

# read density of state
dos = extract_dos('vasprun.xml')[0]
dos [:,0] -= vbm_offset
## uncomment to apply a scissor operator for opening the band gap
#cbm = vbm + e_gap
#cbmi = np.where(dos[:, 0] <= cbm)[0][-1]
#diff_eg = 3.51 - e_gap
#dos[cbmi:, 0] += diff_eg


T_range = np.linspace(250, 1000, 100)

concentrations = EquilibriumConcentrations(charge_states, form_energy_vbm,
                                           vbm, e_gap, site_conc, dos, T_range)
carriers = concentrations.equilibrium_carrier_concentrations
T_plot = 1000/T_range

plt.rcParams.update({'font.size': 24})
plt.rcParams['figure.figsize'] = (10, 8)

plt.plot(T_plot, np.abs(carriers), linewidth=3)
plt.yticks([1e-10, 1, 1e10, 1e20])
plt.xlim(1, 4)
plt.yscale('log')
plt.title('Carrier concentrations')
plt.xlabel('1000/T (1/K)')
plt.ylabel(r'c (cm$^{-3}$)')
plt.tight_layout()
plt.savefig('carriers.png')
plt.show()

fermi = concentrations.equilibrium_fermi_level - concentrations.vbm
plt.plot(T_plot, fermi, linewidth=3)
plt.title('Equilibrium Fermi level')
plt.xlabel('1000/T (1/K)')
plt.ylabel(r'$E_F$ (eV)')
plt.xlim(1, 4)
plt.tight_layout()
plt.savefig('fermi_level.png')
plt.show()

def_concs = concentrations.equilibrium_defect_concentrations
plt.plot(T_plot, def_concs['Vac_N'][1], color='blue', linewidth=3, label=r'$Vac_N$ +1')
plt.plot(T_plot, def_concs['Vac_N'][2], color='blue', linewidth=3, linestyle='--', label=r'$Vac_N$ +2')
plt.title('Defect concentrations')
plt.xlabel('1000/T (1/K)')
plt.xticks([1,2,3,4])
plt.xlim(1, 4)
plt.yscale('log')
plt.ylabel(r'c (cm$^{-3}$)')
plt.legend()
plt.tight_layout()
plt.savefig('def_concentrations.png')
plt.show()

df = concentrations.defect_concentrations_dataframe
carriers_df = concentrations.carrier_concentrations_dataframe

import pandas as pd
new_df = pd.concat([df, carriers_df], axis=1)
# show that free electron are mostly generated by single ionization of Vac_N
data = new_df.loc[:, [('Vac_N', 1), 'electron']].values
plt.plot(T_range, (data[:, 1] - data[:, 0])/data[:, 1], linewidth=3)
plt.title('Relative electron concentrations')
plt.xlabel('T (K)')
plt.xticks([250, 500 , 750 , 1000])
plt.xlim(250, 1000)
plt.ylabel(r'$\frac{c(e^-) - c(Vac_N^+)}{c(e^-)}$')
plt.tight_layout()
plt.savefig('rel_concentrations.png')
plt.show()
