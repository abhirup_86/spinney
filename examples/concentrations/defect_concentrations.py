import numpy as np
import os
import ase.io
from spinney.structures.defectivesystem import DefectiveSystem
from spinney.tools.formulas import count_elements
from spinney.io.vasp import extract_dos

path_defects = os.path.join('..', 'diagram', 'data', 'data_defects')
path_pristine = os.path.join('..', 'diagram', 'data', 'pristine', 'OUTCAR')
path_ga = os.path.join('..', 'diagram', 'data', 'Ga', 'OUTCAR')

ase_ga = ase.io.read(path_ga, format='vasp-out')

# Band alignment
vbm_offset = 0.85
vbm = 5.009256 - vbm_offset # align the VBM with the HSE band
e_gap = 1.713

# dielectric tensor
e_rx = 5.888338 + 4.544304
e_rz = 6.074446 + 5.501630
e_r = [[e_rx, 0, 0], [0, e_rx, 0], [0, 0, e_rz]]

# get the chemical potential of Ga
ase_pristine = ase.io.read(path_pristine, format='vasp-out')
chem_pot_ga = ase_ga.get_total_energy()/len(ase_ga)
# get the chemical potential of N in the Ga-rich conditions
elements = count_elements(ase_pristine.get_chemical_formula())
chem_pot_n = ase_pristine.get_total_energy() - elements['Ga']*chem_pot_ga
chem_pot_n /= elements['N']

# get the density of states
dos = extract_dos('vasprun.xml')[0]
dos [:,0] -= vbm_offset

# site concentrations for point defects
volume = ase_pristine.get_volume()/len(ase_pristine)
volume *= 4
factor = 1e-8**3 * volume
factor = 1/factor
site_conc = {'Ga_N':4, 'N_Ga':4, 'Vac_N':4, 'Vac_Ga':4,
             'Ga_int':6, 'N_int':6, 'electron':36 , 'hole':36}
site_conc = {key:value*factor for key, value in site_conc.items()}

defective_system = DefectiveSystem(os.path.join('..', 'diagram', 'data'),
                                   'vasp')
defective_system.vbm = vbm
defective_system.dielectric_tensor = e_r
defective_system.chemical_potentials = {'Ga':chem_pot_ga, 'N':chem_pot_n}
defective_system.correction_scheme = 'ko'
# specific data for obtaining the EquilibriumDefectConcentration object
defective_system.gap_range = (vbm, vbm + e_gap)
defective_system.site_concentrations = site_conc
defective_system.temperature_range = np.linspace(250, 1000, 100)
defective_system.dos = dos
defective_system.calculate_energies(False)

concentrations = defective_system.concentrations

import matplotlib.pyplot as plt

T_plot = 1000/defective_system.temperature_range
carriers = concentrations.equilibrium_carrier_concentrations
plt.rcParams.update({'font.size': 24})
plt.rcParams['figure.figsize'] = (10, 8)

plt.plot(T_plot, np.abs(carriers), linewidth=3)
plt.yticks([1e-10, 1, 1e10, 1e20])
plt.xlim(1, 4)
plt.yscale('log')
plt.title('Carrier concentrations')
plt.xlabel('1000/T (1/K)')
plt.ylabel(r'c (cm$^{-3}$)')
plt.tight_layout()
plt.show()

fermi = concentrations.equilibrium_fermi_level - concentrations.vbm
plt.plot(T_plot, fermi, linewidth=3)
plt.title('Equilibrium Fermi level')
plt.xlabel('1000/T (1/K)')
plt.ylabel(r'$E_F$ (eV)')
plt.xlim(1, 4)
plt.tight_layout()
plt.show()

def_concs = concentrations.equilibrium_defect_concentrations
plt.plot(T_plot, def_concs['Vac_N'][1], color='blue', linewidth=3, label=r'$Vac_N$ +1')
plt.plot(T_plot, def_concs['Vac_N'][2], color='blue', linewidth=3, linestyle='--', label=r'$Vac_N$ +2')
plt.title('Defect concentrations')
plt.xlabel('1000/T (1/K)')
plt.xticks([1,2,3,4])
plt.xlim(1, 4)
plt.yscale('log')
plt.ylabel(r'c (cm$^{-3}$)')
plt.legend()
plt.tight_layout()
plt.savefig('def_concentrations.png')
plt.show()

df = concentrations.defect_concentrations_dataframe
carriers_df = concentrations.carrier_concentrations_dataframe

import pandas as pd
new_df = pd.concat([df, carriers_df], axis=1)
# show that free electron are mostly generated by single ionization of Vac_N
data = new_df.loc[:, [('Vac_N', 1), 'electron']].values
plt.plot(concentrations.T, (data[:, 1] - data[:, 0])/data[:, 1], linewidth=3)
plt.title('Relative electron concentrations')
plt.xlabel('T (K)')
plt.show()
