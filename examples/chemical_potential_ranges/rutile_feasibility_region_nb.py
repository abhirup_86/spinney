#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from spinney.thermodynamics.chempots import Range, get_chem_pots_conditions

if __name__ == '__main__':
    data = 'formation_energies.txt'

    equality_compounds = ['TiO2_rutile']
    order = ['O', 'Ti']
    parsed_data= get_chem_pots_conditions(data, order, equality_compounds)         
    # prepare Range instances
    crange = Range(*parsed_data[:-1])

    print(crange.variables_extrema)

    crange.set_compound_dict(parsed_data[-1])
    # let's use some pretty labels in the plot
    # the order of the axes  must follow the order used for get_chem_pots_conditions
    labels = [r'$\Delta \mu_{O}$ (eV)', r'$\Delta \mu_{Ti}$ (eV)']
    crange.plot_feasible_region_on_plane([0, 1], x_label=labels[0],
                                         y_label=labels[1],
                                         title='Rutile phase', save_plot=True)
    mu_o_ti_rich = crange.variables_extrema[0, 0]
    mu_ti_ti_rich = crange.variables_extrema[1, 1]

    data_nb = 'formation_energies_with_nb.txt'

    equality_compounds = ['TiO2_rutile']
    order = ['O', 'Ti', 'Nb']
    parsed_data= get_chem_pots_conditions(data_nb, order, equality_compounds)         
    # prepare Range instances
    crange_nb = Range(*parsed_data[:-1])

    print(crange_nb.variables_extrema)

    crange_nb.set_compound_dict(parsed_data[-1])
    # let's use some pretty labels in the plot
    # the order of the axes  must follow the order used for get_chem_pots_conditions
    labels = [r'$\Delta \mu_{O}$ (eV)', r'$\Delta \mu_{Ti}$ (eV)', r'$\Delta \mu_{Nb}$ (eV)']
    # Nb-rich conditions
    crange_nb.plot_feasible_region_on_plane([0, 1], x_label=labels[0],
                                            y_label=labels[1],
                                            plane_values = [0],
                                            y_val_min = -10, # minimum value second variable
                                            title='Rutile phase, Nb doping, Nb-rich',
                                            save_plot=True,
                                            save_title='feasibility_region_Nb_rich')

    nb_ti_rich = (-28.9191827617 - 7*mu_o_ti_rich - mu_ti_ti_rich)/2
    crange_nb.plot_feasible_region_on_plane([0, 1], x_label=labels[0],
                                            y_label=labels[1],
                                            plane_values = [nb_ti_rich],
                                            y_val_min = -10,
                                            title='Rutile phase, Nb doping, Ti-rich', save_plot=True,
                                            save_title='feasibility_region_Ti_rich')

    crange_nb.plot_feasible_region_on_plane([1, 2], x_label=labels[1],
                                            y_label=labels[2],
                                            plane_values = [0],
                                            title='Rutile phase, Nb doping, O-rich', save_plot=True,
                                            save_title='feasibility_region_O_rich')
    print(crange_nb.variables_extrema)
    print(crange_nb.variables_extrema_2d)
