import os

import ase.io
from spinney.structures.defectivesystem import DefectiveSystem


defective_system = DefectiveSystem('data', 'vasp')

# values chemical potentials from standard state
dmu_ga = 0
dmu_mg = -0.668

# prepare chemical potentials with proper values
# paths with calculations results
path_defects = os.path.join('data', 'data_defects')
path_pristine = os.path.join('data', 'pristine', 'OUTCAR')
path_ga = os.path.join('data', 'Ga', 'OUTCAR')
path_mg = os.path.join('data', 'Mg', 'OUTCAR')
# calculate chemical potentials of pure phases
ase_pristine = ase.io.read(path_pristine, format='vasp-out')
mu_prist = 2*ase_pristine.get_total_energy()/len(ase_pristine)
ase_ga = ase.io.read(path_ga, format='vasp-out')
mu_ga = ase_ga.get_total_energy()/len(ase_ga) # Ga-rich 
ase_mg = ase.io.read(path_mg, format='vasp-out')
mu_mg = ase_mg.get_total_energy()/len(ase_mg) # Mg-rich 
mu_n = mu_prist - mu_ga # N-poor
mu_ga += dmu_ga
mu_mg += dmu_mg

defective_system.chemical_potentials = {'Ga':mu_ga, 'N':mu_n, 'Mg':mu_mg}

vbm = 5.009256
e_rx = 5.888338 + 4.544304
e_rz = 6.074446 + 5.501630
e_r = [[e_rx, 0, 0], [0, e_rx, 0], [0, 0, e_rz]]

defective_system.vbm = vbm
defective_system.dielectric_tensor = e_r
defective_system.correction_scheme = 'ko'
defective_system.calculate_energies(False)
df = defective_system.data
print(df)
defective_system.write_formation_energies('formation_energies_Mg_GaN.txt')

# prepare the diagram
defective_system.gap_range = (0, 1.713) 
#defective_system.extended_gap_range = (-0.85, - 0.85 + 3.51)
dgm = defective_system.diagram
dgm.plot(save_flag=True, title='Mg-doped GaN Ga-rich limit', legend=True,
         x_label=r'$E_F$ (eV)', save_title='diagram_defsys')
print(dgm.transition_levels)
# defect concentrations
from spinney.io.vasp import extract_dos
import numpy as np

# get the density of states of the pristine system
dos = extract_dos('vasprun.xml')[0]

# site concentrations for point defects
volume = ase_pristine.get_volume()/len(ase_pristine)
volume *= 4
factor = 1e-8**3 * volume
factor = 1/factor
site_conc = {'Ga_N':4, 'N_Ga':4, 'Vac_N':4, 'Vac_Ga':4,
             'Ga_int':6, 'N_int':6, 'Mg_Ga':4,
             'electron':36 , 'hole':36}
site_conc = {key:value*factor for key, value in site_conc.items()}

defective_system.dos = dos
defective_system.site_concentrations = site_conc
# calculate defect concentrations on a range of temperature
defective_system.temperature_range = np.linspace(250, 1000, 100)
    
concentrations = defective_system.concentrations

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 24})

carriers = concentrations.equilibrium_carrier_concentrations

# the sign is positive, except for very low temperatures:
# holes are the main carriers
T_plot = 1000/defective_system.temperature_range
plt.plot(T_plot, np.abs(carriers), linewidth=3)
plt.yticks([1e-10, 1, 1e10, 1e20])
plt.xlim(1, 4)
plt.yscale('log')
plt.title('Carrier concentrations')
plt.xlabel('1000/T (1/K)')
plt.ylabel(r'c (cm$^{-3}$)')
plt.tight_layout()
plt.savefig('carriers.png')
plt.show()


