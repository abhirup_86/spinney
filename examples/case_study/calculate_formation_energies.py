import numpy as np
import os

import ase.io

from spinney.tools.reactions import calculate_formation_energy_fu
from spinney.tools.formulas import get_formula_unit, count_elements

def calculate_formation_energy(path, compound_name):
    compound_path = os.path.join(path, compound_name, 'OUTCAR')
    atoms = ase.io.read(compound_path)
    energy = atoms.get_total_energy()
    formula = atoms.get_chemical_formula()
    compound_dict = {formula:energy}
    # find parents
    ele = count_elements(formula)
    components_dict = {}
    for el in ele.keys():
        parent = os.path.join(path, el, 'OUTCAR')
        parent = ase.io.read(parent)
        components_dict[parent.get_chemical_formula()] = parent.get_total_energy() 
    return calculate_formation_energy_fu(compound_dict, components_dict)
    
    
if __name__ == '__main__':
    path = 'data_chull'
    path_mg = os.path.join(path, 'Mg')
    path_n2 = os.path.join(path, 'N')
    path_ga = os.path.join(path, 'Ga')

    f = open('formation_energies.txt', 'w')
    f.write('# Formation energies per formula unit calculated with PBE for GaN\n'
            '#Compound                 E_f (eV/fu)\n')
    for root, dirs, files in os.walk(path):
        if not dirs:
            compound = root.split(os.sep)[-1]
            energy = calculate_formation_energy(path, compound)
            f.write('{:<8} {:30.10f}\n'.format(compound, energy))
    f.close()
