import os

import ase.io
from spinney.structures.pointdefect import PointDefect
from spinney.io.vasp import extract_potential_at_core_vasp
from spinney.tools.formulas import count_elements

path_defects = os.path.join('data', 'data_defects')
path_pristine = os.path.join('data', 'pristine', 'OUTCAR')
path_ga = os.path.join('data', 'Ga', 'OUTCAR')

# prepare preliminary data
ase_pristine = ase.io.read(path_pristine, format='vasp-out')
pot_pristine = extract_potential_at_core_vasp(path_pristine)
ase_ga = ase.io.read(path_ga, format='vasp-out')

vbm = 5.009256
e_rx = 5.888338 + 4.544304
e_rz = 6.074446 + 5.501630
e_r = [[e_rx, 0, 0], [0, e_rx, 0], [0, 0, e_rz]]

# store defect positions in  dictionary for access convenience
defect_positions = {'Ga_int' : (0.25, 0.55, 0.55), 
                    'N_int'  : (0.5, 0.41667, 0.50156),
                    'Vac_Ga' : (0.5, 0.6666666666, 0.50156),
                    'Vac_N'  : (0.5, 0.3333333333, 0.46045),
                    'Ga_N'   : (0.5, 0.3333333333, 0.46045),
                    'N_Ga'   : (0.5, 0.6666666666, 0.50156)
                    }

# get the chemical potential of Ga
chem_pot_ga = ase_ga.get_total_energy()/len(ase_ga)
# get the chemical potential of N in the Ga-rich conditions
elements = count_elements(ase_pristine.get_chemical_formula())
chem_pot_n = ase_pristine.get_total_energy() - elements['Ga']*chem_pot_ga
chem_pot_n /= elements['N']

# write the formation energy for Fermi level = 0 to a file
energy_file = open('formation_energies_GaN_Ga_rich.txt', 'w')
energy_file.write('#{:<8} {:8} {:>30}\n'.format('system', 
                                                'charge',
                                                'formation energy (eV)'))
for root, dirs, files in os.walk(path_defects):
    path = root.split(os.sep)
    if 'OUTCAR' in files:
        def_path = os.path.join(root, 'OUTCAR')
        ase_outcar = ase.io.read(def_path)
        pot_defective = extract_potential_at_core_vasp(def_path)
        defect_name = path[-2]
        charge_state = int(path[-1])
        print('Processing defect {} in charge state {}'.format(defect_name,
                                                               charge_state))
        print('Data in: {}'.format(path))
        # prepare PointDefect object
        pdf = PointDefect(ase_outcar)
        pdf.set_pristine_system(ase_pristine)
        pdf.set_chemical_potential_values({'N':chem_pot_n, 'Ga':chem_pot_ga})
        pdf.set_vbm(vbm)
        pdf.set_defect_charge(charge_state)
        pdf.set_defect_position(defect_positions[defect_name])
        pdf.set_dielectric_tensor(e_r)
        pdf.set_finite_size_correction_scheme('ko')
        pdf.add_correction_scheme_data(potential_pristine=pot_pristine,
                                       potential_defective=pot_defective)
        corrected_energy = pdf.get_defect_formation_energy(True)
        energy_file.write('{:<8} {:>8} {:30.10f}\n'.format(defect_name,
                                                      charge_state,
                                                      corrected_energy))
        print('Done.\n------------------------------------------------------')

energy_file.close()


from spinney.defects.diagrams import extract_formation_energies_from_file, Diagram

data_file = 'formation_energies_GaN_Ga_rich.txt'
defect_dictionary = extract_formation_energies_from_file(data_file)
dgm = Diagram(defect_dictionary, (0, 1.713))
dgm.write_transition_levels('transition_levels.txt')
# use some prettier labels in the plot
dgm.labels = {'Ga_int' : r'$Ga_i$',
              'N_int'  : r'$N_i$',
              'Vac_Ga' : r'$Vac_{Ga}$',
              'Vac_N'  : r'$Vac_N$',
              'Ga_N'   : r'$Ga_N$',
              'N_Ga'   : r'$N_{Ga}$'}
# select colors to use in the plot
colors = {'Ga_int' : 'red', 
          'N_int'  : 'blue',
          'Vac_Ga' : 'orange',
          'Vac_N'  : 'gray',
          'Ga_N'   : 'magenta',
          'N_Ga'   : 'green'}
dgm.plot(save_flag=True, save_title='diagram1',
         title='Ga-rich limit', legend=True,
         colors_dict=colors, x_label=r'$E_F$ (eV)')
dgm = Diagram(defect_dictionary, (0, 1.713), (-0.85, - 0.85 + 3.51))
dgm.write_transition_levels('transition_levels_extended.txt')
# use some prettier labels in the plot
dgm.labels = {'Ga_int' : r'$Ga_i$',
              'N_int'  : r'$N_i$',
              'Vac_Ga' : r'$Vac_{Ga}$',
              'Vac_N'  : r'$Vac_N$',
              'Ga_N'   : r'$Ga_N$',
              'N_Ga'   : r'$N_{Ga}$'}
dgm.plot(save_flag=True, save_title='diagram2',
         title='Intrinsic GaN, Ga-rich conditions', legend=True,
         colors_dict=colors, x_label=r'$E_F$ (eV)')
