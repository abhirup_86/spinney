__all__ = ['__author__',
           '__email__',
           '__title__',
           '__version__',
           '__summary__',
           '__uri__',
           '__copyright__',
           '__license__'
          ]

__author__ = 'Marco Arrigoni'

__email__ = 'marco.arrigoni@outlook.de'

__title__ = 'spinney'

__version__ = '0.9.a2'

__summary__ = 'Spinney: a Python package for assisting with first-principles simulations of point defects in solids.'

__uri__ = 'https://gitlab.com/Marrigoni/spinney'

__copyright__ = '2019-2020'

__license__ = 'MIT'

