.. _tutorial:

Tutorial
********

The properties of many materials can be strongly affected by the introduction of impurity atoms.
For example, the doping of semiconductor materials lies at the foundation electronics industry.

The amount of impurities sufficient to significantly alter the property of materials is in
general in the ppm. Intrinsic defects, which are ubiquitous in every material, also generally
appear in this concentration range.

For such small concentrations, it can be assumed that each defect will not feel the presence of
other defects. Despite the truth of this claim should be carefully assessed on a case-by-case
basis - defect complexes do occurr and concentrations of intrinsic defects might be considerable, as
in the case of nonstoichiometric compounds - defect mutual independence, that is considering the 
*dilute limit*, is generelly assumed since it allows to easily obtains relevant properties of
defective systems, such as defect and carrier concentrations.

:program:`Spinney` implements the formulism devised for point defects in the *dilute limit*,
it is also assumed that first-principles calculations of point defects are performed within the supercell
formalism :cite:`1992:Payne`, which is currently the most widely employed.

This tutorial will review the most common steps involved in the study of point defects in solids using
first-principles simulations and will show how :program:`Spinney` can be used to assist with the investigation.


.. toctree::
    :maxdepth: 2  
    :caption: Contents
    
    defform
    corrschemes
    chemipots
    diagrams
    concentrations


