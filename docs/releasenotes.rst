.. _release_notes:

Release Notes
*************
Version 0.9.a1
==============

16 October 2020:

- Updated contacts emails.

Version 0.9.a1
==============

21 July 2020:

- The package is now compatible also with Windows 10.

Version 0.9.a0
==============

25 June 2020:

- Fixed some small bugs that gave problem in plotting the feasible region with
  newer versions of scipy.

Version 0.8.a3
==============

27 February 2020:

- Added some internal helper functions.
- Fixed the sorting in the pandas series used for storing charge transition levels. 

Version 0.8.a2
==============

20 January 2020:

- Improved the method for plotting the intersection of the feasible region
  with the plane defined by constant chemical potentials.
- Added a complete case study in the tutorial.
- Small bug fixes.

Version 0.8.a1
==============

7 January 2020:

- The class ``DefectiveSystem`` can be used for obtaining equilibrium
  defect concentrations.

Version 0.8.a0
==============

18 December 2019:

- **Added compatibility with**  `ASE` **version >= 3.18.0.**
- Added the class ``DefectiveSystem`` to manage defect formation energy
  calculations for a system with different point defects.

Version 0.7.a5
==============

19 November 2019:

- First version released to the public.

